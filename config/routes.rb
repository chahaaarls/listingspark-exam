Rails.application.routes.draw do
  devise_for :users

  root to: 'properties#index'
  resources :properties, only: [:index, :create, :update]

  get 'properties/:id', to: 'properties#index'
  get 'properties/actions/update_details_fields', to: 'properties#update_details_fields'
end
