class Property
  include Mongoid::Document

  validates :address, :presence => true
  belongs_to :user

  field :state, type: String
  field :address, type: String
  field :roof_type, type: String
  field :selling, type: Boolean, default: false
  field :details, type: Hash

  def get_status
    status = (self.selling == true)? 'For Sale': 'Pending'
  end

  def get_completeness
    completeness = ((4 - self.get_missing_fields.count)/4.0 * 100).to_int
  end

  def get_missing_fields
    missing_fields = []

    if self.roof_type.blank?
      missing_fields.push(:roof_type)
    end

    unless self.details.nil?
      self.details.each do |key, value|
        if value.blank?
          missing_fields.push(key)
        end
      end
    end

    return missing_fields
  end

  private
    def details_empty
      if self.details.nil?
        errors.add("details", "cannot be empty")
      else
        self.details.each do |k, v|
          if v.blank?
            errors.add(k, "cannot be empty")
          end
        end
      end
    end
end
