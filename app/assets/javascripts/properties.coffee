$ ->
  $(document).on 'change', '#property_state', (event) ->
    state = $('#property_state :selected').text();

    $.ajax '/properties/actions/update_details_fields',
      type: 'GET'
      dataType: 'script'
      data: {state: state}
