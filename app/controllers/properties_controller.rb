class PropertiesController < ApplicationController
  before_action :authenticate_user!

  def index
    prop_id = params[:id]
    property = nil
    state = nil

    if prop_id
      property = Property.find(prop_id)
      state = property.state
    else
      property = Property.new
    end

    initialize_vars(property, state)

    @current_tab = 'property'
  end

  def create
    @property = Property.new(property_params)
    @property.user = current_user

    if @property.save
      initialize_vars(@property, @property[:state])

      @current_tab = 'sell'
      # flash.now[:success] = "Property created successfully"
    else
      initialize_vars(@property, @property[:state])

      @current_tab = 'property'
      # flash.now[:danger] = "Failed to create property"
    end

    render 'index'
  end

  def update
    @property = Property.find(params[:id])
    @current_tab = params[:property][:current_tab]

    if @current_tab == 'sell'
      if @property.get_missing_fields.any?
        initialize_vars(@property, @property[:state])
        @property.errors.add('Property', 'should be 100% complete to be submitted for selling')

        render 'index'
        return
      end
    end

    if @property.update_attributes(property_params)
      if @current_tab == 'sell'
        @current_tab = 'properties'
        @property = Property.new
      else
        @current_tab = 'sell'
      end

      initialize_vars(@property, @property[:state])
    else
      initialize_vars(@property, @property[:state])
    end

    render 'index'
  end

  def update_details_fields
    initialize_vars(Property.new, params[:state])
  end

private
  def initialize_vars(property, state)
    @properties = current_user.properties.all
    @property = property
    @states = get_property_states

    state = params.try(:fetch, :state, @states.first) if state.nil? else state

    @details_fields = get_details_fields(state)
  end

private
  def get_property_states
    PropertyConfig::PROPERTY_DETAILS_FIELDS.keys.sort
  end

private
  def get_details_fields(state)
    details = PropertyConfig::PROPERTY_DETAILS_FIELDS
    details.try(:fetch, state, {}).try(:fetch, 'details', {})
  end

private
  def property_params
    state = params[:property].try(:fetch, :state, '')
    details_keys = get_details_fields(state).keys

    params.require(:property).permit(:state, :address, :roof_type, :selling, details: details_keys)
  end
end
